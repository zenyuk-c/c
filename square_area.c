/*
  Calculate max square out of the input below: (answer 6, column 4 & 5 have common height of 3 -> 2 * 3 = 6)

        X
  X     X X
  X X   X X 
  X X X X X
*/

#include <stdlib.h>
#include <stdio.h>

typedef struct {
  int begin;
  int end;
  char emtpy;
} Range;

void sortedCopy(int arr[], int arrLength, int arrCopy[]) {
  // first cut will be 0
  arrCopy[0] = 0;
  for (int i = 1; i < arrLength; ++i) {
    arrCopy[i] = arr[i];
  }
  int notSortedLength = arrLength;
  int temp = arrCopy[0];
  for (int i = 0; i < notSortedLength; ++i) {
    for (int swapIdx = 0; swapIdx < notSortedLength - 1; ++swapIdx) {
      if (arrCopy[swapIdx] > arrCopy[swapIdx + 1]) {
        temp = arrCopy[swapIdx];
        arrCopy[swapIdx] = arrCopy[swapIdx + 1];
        arrCopy[swapIdx + 1] = temp;
      }
    }
    --notSortedLength;
  }
}

int min(int arr[], int arrLength, Range range) {
  if (range.begin < 0 || range.end >= arrLength || range.begin > range.end) {
    printf("ERROR: invalid parameters to calculate min\n");
    exit(1);
  }

  if (range.begin == range.end)
    return arr[range.begin];

  int min = arr[range.begin];
  for (int i = range.begin + 1; i <= range.end; ++i) {
    if (arr[i] < min)
      min = arr[i];
  }
  return min;
}

int firstFilledInFromPosition(int arr[], int arrLength, int position, int cutHeight) {
  for (int i = position; i < arrLength; ++i) {
    if (arr[i] - cutHeight > 0) {
      return i;
    }
  }
  return -1;
}

int firstEmptyFromPosition(int arr[], int arrLength, int position, int cutHeight) {
  for (int i = position; i < arrLength; ++i) {
    if (arr[i] - cutHeight <= 0) {
      return i;
    }
  }
  // empty element was not found
  return -1;
}

Range cutNextBlock(int arr[], int arrLength, int position, int cutHeight) {
  Range result = { .begin = -1, .end = -1, .emtpy = 0};
  int first = -1;
  first = firstFilledInFromPosition(arr, arrLength, position, cutHeight);
  if (first < 0) {
    result.emtpy = 1;
    return result;
  }
  result.begin = first;
  result.end = first;

  int last = -1;
  last = firstEmptyFromPosition(arr, arrLength, first, cutHeight);
  if (last < 0) {
    // went all the way to the end
    result.end = arrLength - 1;
  } else {
    result.end = last - 1;
  }

  return result;
}

// calculate cutted from the bottom block without gaps
int blockArea(int arr[], int arrLength, Range range) {
  int minimal = min(arr, arrLength, range);
  return minimal * (range.end - range.begin + 1);
}

int maxBlock(int arr[], int arrLength, int cutHeight) {
  int result = 0;
  int position = 0;
  Range nextBlock;
  int nextBlockArea;
  while (position < arrLength) {
    nextBlock = cutNextBlock(arr, arrLength, position, cutHeight);
    if (nextBlock.emtpy) {
      break;
    }
    nextBlockArea = blockArea(arr, arrLength, nextBlock);
    printf("nextBlock: %d %d %d\n", nextBlock.begin, nextBlock.end, nextBlockArea);
    if (nextBlockArea > result) {
      result = nextBlockArea;
    }
    position = nextBlock.end + 1;
  }
  return result;
}

int HistogramArea(int arr[], int arrLength) {
  if (arrLength < 1)
    return 0;
  
  if (arrLength == 1)
    return arr[0]; 

  int sortedArr[arrLength + 1];
  sortedCopy(arr, arrLength, sortedArr);
  int maxArea = 0;
  int currentArea = 0;
  for (int i = 0; i < arrLength; ++i) {
    //TODO: skip duplicate heights
    currentArea = maxBlock(arr, arrLength, sortedArr[i]);
    if (currentArea > maxArea) {
      maxArea = currentArea;
    }
  }
  return maxArea;
}

int main(void) { 
  int arr[6] = {6, 3, 1, 4, 12, 4};
  int arrLength = 6;
  printf("final: %d, \n", HistogramArea(arr, arrLength));

  return 0;
    
}




