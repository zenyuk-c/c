#include <stdio.h>

int main() {
	int x = 5;
	int y = 7;
	if (x > y) {
		printf("x is bigger\n");
	} else {
		printf("y is bigger then x; y > 6 ?%d\n", (y > 6)?y:6);
	}

	switch(y) {
		case 6:
			printf("y is 6\n");
			break;
		case 7:
			printf("y is 7\n");
			break;
		default:
			printf("y is someting else: %d\n", y);
	}
}
