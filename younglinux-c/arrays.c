#include <stdio.h>

#define SIZE 100

int main() {
    char letters[] = {'a', 'b', 'c'};
    int numbers[SIZE];

    numbers[0] = 25;
    numbers[4] = 57;

    printf("%c %d\n", letters[1], numbers[4]);
    printf("undefined value: %d\n", numbers[3]);
}
