#include <stdio.h>

int main() {
	int x = 0;
	int y = 0;
	printf("x=%d y=%d\n", x, y);
	y = x++;
	printf("x=%d y=%d\n", x, y);

	float f1 = 0.0;
	float f2 = 0.0;
	printf("f1=%g f2=%g\n", f1, f2);

	f1++;
	f2--;	
	printf("f1=%g f2=%g\n", f1, f2);
}
