#include <stdio.h>
int main() {
	printf("|555|\n");
	printf("|");
	printf("%10d", 555);
	printf("|\n");
	
	printf("|");
	printf("%-10d", 555);
	printf("|\n");

	printf("Words:");
	printf("%10d\n", 59);

	printf("Letters:");
	printf("%8d\n", 1004);

	printf("Digits:");
	printf("%9d\n", 8);

}
