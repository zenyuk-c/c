#include <stdio.h>

int main() {
    float f = 12.345;
    double d = 12.345;
    printf("%.2f\n", f);
    printf("%g\n", d);
    printf("%e\n", d);
}
