#include <stdio.h>

int main() {
	long ld = 5.0;
	printf("size long: %d\n", sizeof(long));
	printf("size long long: %d\n", sizeof(long long));
	printf("size double: %d\n", sizeof(double));
	printf("size long double: %d\n", sizeof(long double));
	printf("size int: %d\n", sizeof(int));
}
