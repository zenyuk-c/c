#include <stdio.h>

int main(int argc, char *argv[]) 
{
	const char *version1 = "string_literal";
	printf("string literal: %lu\n", sizeof(version1));

	char version2[] = "char_array";
	printf("char array: %lu\n", sizeof(version2));

	return 0;
}
